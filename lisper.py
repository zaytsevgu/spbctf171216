import idaapi

cit_strgs = {
	70:'cit_empty',
	71:'cit_block',
	72:'cit_expr',
	73:'cit_if',
	74:'cit_for',
	75:'cit_while',
	76:'cit_do',
	77:'cit_switch',
	78:'cit_break',
	79:'cit_continue',
	80:'cit_return',
	81:'cit_goto',
	82:'cit_end',	
	
}

class mytest(idaapi.ctree_parentee_t):
    def __init__(self):
        super(mytest, self).__init__()
	self.storage = []

    def visit_insn(self, instruction):
	self.storage.append(instruction)
        return 0


class AstLisper(object):

    def parseCBlock(self, inst):
        print "In cblock"
        ret = "(cblock"
        for i in xrange(inst.cblock.size()):
	    ret += " " + self.parseInstruction(inst.cblock.at(i))
        return ret + ")"	


    def parseCWhile(self, inst):
        ret = "(while "
        ret += self.parseExpr(inst.cwhile.expr)
        ret += " "
        ret += self.parseInstruction(inst.cwhile.body)
        ret += ")"
        return ret

    def parseCReturn(self, inst):
        ret = "(return "
        ret += self.parseExpr(inst.creturn.expr)
        ret += ")"
        return ret

    def parseExpr(self, expr):
        print expr.opname
        if expr.opname in self.exprHandlers:
            return self.exprHandlers[expr.opname](expr)
        ret =  "(%s" % (expr.opname)
        for i in sorted(expr.operands.keys()):
            curr = expr.operands[i]
            print "key: " + i
            print "elem " + str(curr)
            tp = type(curr)
            print tp
            if tp == idaapi.cexpr_t:
                ret += " " +self.parseExpr(curr)
            elif tp == type(123) or tp == type(1L):
                if i == 'obj_ea':
                    ret += " "+Name(curr)
                else:
                    ret += " %x" % (curr)
        ret += ")"
        return ret

    def parseExprCall(self, expr):
        ret = "(call "
        ret += self.parseExpr(expr.x)
        ret += ")"
        return ret
    
    def parseExprVar(self, expr):
        ret = "(var "
        ret += self.decom.lvars[expr.v.idx].name
        ret += ")"
        return ret

    def parseExprCast(self, expr):
        ret = "(cast `"+expr.type.dstr()+"` "
        ret += self.parseExpr(expr.x)
        ret += ")"
        return ret

    def parseExprNum(self, expr):
        ret = "(num "
        tp = tinfo_t(0x20)
        ret += "%x" % expr.n.value(tp)
        ret += ")"
        return ret

    def parseCExpr(self, instr):
        return self.parseExpr(instr.cexpr)


    def parseCif(self, instr):
        ret = "(if "
        ret += self.parseExpr(instr.cif.expr)
        ret += " "
        print "PARSING IF TRUE BRANCH"
        ret += self.parseInstruction(instr.cif.ithen)
        print "END PARSING"
        if instr.cif.ielse is not None:
            ret += " "
            ret += self.parseInstruction(instr.cif.ielse)
        ret += ")"
        return ret

    def __init__(self, decom):
        self.parsed = []
        self.decom = decom
        self.typeHandlers = {
            'block': self.parseCBlock,
            'while': self.parseCWhile,
            'return': self.parseCReturn,
            'expr': self.parseCExpr,
            'if':   self.parseCif
        }
        self.exprHandlers = {
            'call': self.parseExprCall,
            'var' : self.parseExprVar,
            'cast': self.parseExprCast,
            'num' : self.parseExprNum,
        }



    def getInstructionType(self, i):
	return i.opname



    def parseInstruction(self, inst):
        '''
        if inst in self.parsed:
            print "skp"
	    return ""
        '''
	self.parsed.append(inst)
        instType = self.getInstructionType(inst)
        ret = ""
        if instType in self.typeHandlers:
            ret = self.typeHandlers[instType](inst)
        return 	ret


    def getMyLisp(self, instructions):
        prs = ""
	#for i in instructions:
	prs += self.parseInstruction(instructions[0])
	return prs	


def xxx():
	vis = mytest()
	z = decompile(here())
	vis.apply_to(z.body, None)
        ast = AstLisper(z)
	print ast.getMyLisp(vis.storage)


