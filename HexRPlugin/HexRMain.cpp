﻿#include "Common.h"


extern plugin_t PLUGIN;



// Hex-Rays API pointer
hexdsp_t *hexdsp = NULL;

static bool inited = false;
static int state = 0;

static const char * get_event_name(hexrays_event_t event) {
	switch (event) {
		case hxe_flowchart:
			return "hxe_flowchart";
		case hxe_prolog:
			return "hxe_prolog";
		case hxe_preoptimized:
			return "hxe_preoptimized";
		case hxe_locopt:
			return "hxe_locopt";
		case hxe_prealloc:
			return "hxe_prealloc";
		case hxe_glbopt:
			return "hxe_glbopt";
		case hxe_structural:
			return "hxe_structural";
		case hxe_maturity:
			return "hxe_maturity";
		case hxe_interr:
			return "hxe_interr";
		case hxe_combine:
			return "hxe_combine";
		case hxe_print_func:
			return "hxe_print_func";
		case hxe_func_printed:
			return "hxe_func_printed";
		case hxe_resolve_stkaddrs:
			return "hxe_resolve_stkaddrs";
		case hxe_open_pseudocode:
			return "hxe_open_pseudocode";
		case hxe_switch_pseudocode:
			return "hxe_switch_pseudocode";
		case hxe_refresh_pseudocode:
			return "hxe_refresh_pseudocode";
		case hxe_close_pseudocode:
			return "hxe_close_pseudocode";
		case hxe_keyboard:
			return "hxe_keyboard";
		case hxe_right_click:
			return "hxe_right_click";
		case hxe_double_click:
			return "hxe_double_click";
		case hxe_curpos:
			return "hxe_curpos";
		case hxe_create_hint:
			return "hxe_create_hint";
		case hxe_text_ready:
			return "hxe_text_ready";
		case hxe_populating_popup:
			return "hxe_populating_popup";
	}
}


//--------------------------------------------------------------------------
// This callback handles various hexrays events.
static int idaapi callback(void *, hexrays_event_t event, va_list va)
{
	switch (event) {
		case hxe_create_hint:
		{
			vdui_t &ui = *va_arg(va, vdui_t *);
			qstring &result_hint = *va_arg(va, qstring *);
			int *implines = va_arg(va, int *);
			result_hint = "This my test hint\n";
			*implines = 1;
			return 2;
		}
		case hxe_keyboard:
		{
			vdui_t &ui = *va_arg(va, vdui_t *);
			int key = va_arg(va, int);
			msg("Got key %d\n", key);
			if ((key != 73 && (key >= 72) && (key <= 76))) {
				auto viewer = ui.ct;
				int x, y;
				auto pl = get_custom_viewer_place(viewer, false, &x, &y);
				simpleline_place_t *pl2 = static_cast<simpleline_place_t *>(pl);
				switch (key)
				{
				case 72: //H
					jumpto(viewer, pl2, x - 1, y);
					break;
				case 74: //J
					pl2->n++;
					jumpto(viewer, pl2, x, y+1);
					break;
				case 75: //K
					pl2->n--;
					jumpto(viewer, pl2, x, y-1);
					break;
				case 76: //L
					jumpto(viewer, pl2, x + 1, y);
					break;
				}
				return 1;
			}
			return 0;
		}
		case hxe_glbopt:
		{			
			mbl_array_t *a = va_arg(va, mbl_array_t *);
			msg("%x\n", a);
			break;
		}
		case hxe_structural:
		{
			int *ct = va_arg(va, int *);
			msg("%x: %x\n", *ct, *(ct + 1));
		}
		case hxe_combine:
		{

		}
		default:
			msg("Got %s event.\n", get_event_name(event));
			break;
	}
	return 0;
}



//--------------------------------------------------------------------------
// Initialize the plugin.
int idaapi init(void)
{
	msg("\nTest loaded.\n");
	state = 0;
	if (!init_hexrays_plugin())
		return PLUGIN_SKIP; // no decompiler

	install_hexrays_callback(callback, NULL);
	inited = true;
	const char *hxver = get_hexrays_version();
		return PLUGIN_KEEP;
}

//--------------------------------------------------------------------------
void idaapi term(void)
{
	if (inited)
	{
		msg("\nTest plugin unloaded.\n");
		remove_hexrays_callback(callback, NULL);
		term_hexrays_plugin();
	}
}

//--------------------------------------------------------------------------
void idaapi run(int)
{
	state++;
	msg("State2 %d\n", state);
	install_hexrays_callback(callback, NULL);
	if (state == 2)
		PLUGIN.flags |= PLUGIN_UNL;
	
}

//--------------------------------------------------------------------------
static char comment[] = "Test plugin";

//--------------------------------------------------------------------------
//
//      PLUGIN DESCRIPTION BLOCK
//
//--------------------------------------------------------------------------
plugin_t PLUGIN =
{
	IDP_INTERFACE_VERSION,
	0,          // plugin flags
	init,                 // initialize
	term,                 // terminate. this pointer may be NULL.
	run,                  // invoke plugin
	comment,              // long comment about the plugin
						  // it could appear in the status line or as a hint
						  "",                   // multiline help about the plugin
						  "TestPlugin", // the preferred short name of the plugin (PLUGIN.wanted_name)
						  "Alt+F6"                    // the preferred hotkey to run the plugin
};
