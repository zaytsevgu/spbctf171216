import ctypes
import sys
import re

import idaapi

EVENTS_HEXR = {
    0: 'hxe_flowchart', 
    1: 'hxe_prolog', 
    2: 'hxe_preoptimized', 
    3: 'hxe_locopt', 
    4: 'hxe_prealloc', 
    5: 'hxe_glbopt', 
    6: 'hxe_structural', 
    7: 'hxe_maturity', 
    8: 'hxe_interr', 
    9: 'hxe_combine', 
    10: 'hxe_print_func', 
    11: 'hxe_func_printed', 
    12: 'hxe_resolve_stkaddrs', 
    100: 'hxe_open_pseudocode', 
    101: 'hxe_switch_pseudocode', 
    102: 'hxe_refresh_pseudocode', 
    103: 'hxe_close_pseudocode', 
    104: 'hxe_keyboard', 
    105: 'hxe_right_click', 
    106: 'hxe_double_click', 
    107: 'hxe_curpos', 
    108: 'hxe_create_hint', 
    109: 'hxe_text_ready', 
    110: 'hxe_populating_popup'
 }

CMAT_LEVEL = {
        0: 'CMAT_ZERO',
        1: 'CMAT_BUILT',
        2: 'CMAT_TRANS1',
        3: 'CMAT_NICE',
        4: 'CMAT_TRANS2',
        5: 'CMAT_CPA',
        6: 'CMAT_TRANS3',
        7: 'CMAT_CASTED',
        8: 'CMAT_FINAL'
}

LEV = 0
NAME = 'log'

def hexrays_events_callback_m(*args):
    global LEV
    global NAME
    ev = args[0]
    print "Got {}:".format(EVENTS_HEXR[args[0]])
    if ev == idaapi.hxe_maturity:
        print "MATURITY LEVEL {}".format(CMAT_LEVEL[args[2]])
#        #UNCOMMENT IF WANNA SEE FUNCTION BODY CHANGES DURING DECOMPILATION
#        f = open(NAME+str(LEV)+'.txt', 'w')
#        f.write(str(args[1]))
#        LEV += 1
#        f.close()
    elif ev == idaapi.hxe_keyboard:
	key = args[2]
	if key in [72, 74,75,76]:
		viewer = args[1].ct
		(place, x, y) = get_custom_viewer_place(viewer, False)
		pl2 = idaapi.place_t_as_simpleline_place_t(place.clone())
		if key == 72:
			jumpto(viewer, pl2, x-1, y)
		elif key == 74:
			pl2.n -= 1;
			jumpto(viewer, pl2, x, y)
		elif key == 75:
			pl2.n += 1
			jumpto(viewer, pl2, x, y)
		elif key == 76:
			jumpto(viewer, pl2, x+1, y)
		return 1
	return 0
    else:
        for i in args[1:]:
            print "\t{}".format(type(i))
    return 0

def hr_remove():
	idaapi.remove_hexrays_callback(hexrays_events_callback_m)

if __name__ == "__main__":
	print "yay"
	print idaapi.install_hexrays_callback(hexrays_events_callback_m)

