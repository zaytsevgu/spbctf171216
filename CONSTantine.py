import ctypes
import sys
import re

import idaapi

def register(action, *args):
    idaapi.register_action(
        idaapi.action_desc_t(
            action.name,
            action.description,
            action(*args),
            action.hotkey
        )
    )

def unregister(action):
    idaapi.unregister_action(action.name)


class AddConst(idaapi.action_handler_t):

    name = "my:Add const modifier"
    description = "Add const modifier"
    hotkey = "Shift+C"

    def __init__(self):
        idaapi.action_handler_t.__init__(self)

    @staticmethod
    def check(cfunc, ctree_item):
        if ctree_item.citype == idaapi.VDI_EXPR:
            expression = ctree_item.it.to_specific_type

            child = None
            while expression and expression.op not in (idaapi.cot_asg, idaapi.cot_call, idaapi.cot_cast):
                child = expression.to_specific_type
                expression = cfunc.body.find_parent_of(expression)

            if expression:
                expression = expression.to_specific_type
                if expression.op == idaapi.cot_asg:
                       if expression.y.op == idaapi.cot_obj:
                           idaapi.update_action_label(AddConst.name, 'Add const')
                           typ =  expression.y.type.dstr()
                           return expression.y.obj_ea, typ
                if expression.op == idaapi.cot_cast:
                    if expression.x.op == idaapi.cot_obj:
                        return expression.x.obj_ea, expression.x.type.dstr()
                elif expression.op == idaapi.cot_call:
                     for i in expression.a:
                         exp = i.cexpr
                         if exp.op == idaapi.cot_obj:
                             if exp.type.dstr()[:5].lower() == "char[" or exp.type.dstr()[:8].lower() == "wchar_t[":
                                  return exp.obj_ea, exp.type.dstr()
            



    def activate(self, ctx):
        hx_view = idaapi.get_tform_vdui(ctx.form)
        result = self.check(hx_view.cfunc, hx_view.item)

        if result:
            SetType(result[0], "const "+result[1])
            hx_view.refresh_view(True)


    def update(self, ctx):
        if ctx.form_title[0:10] == "Pseudocode":
            return idaapi.AST_ENABLE_FOR_FORM
        return idaapi.AST_DISABLE_FOR_FORM


def hexrays_events_callback_my(*args):

    hexrays_event = args[0]

    if hexrays_event == idaapi.hxe_populating_popup:
        form, popup, hx_view = args[1:]
        item = hx_view.item  # current ctree_item_t

        if AddConst.check(hx_view.cfunc, item):
            idaapi.attach_action_to_popup(form, popup, AddConst.name, None)

    return 0


if __name__ == "__main__":
    print "12"
    print unregister(AddConst)
    print register(AddConst)
    idaapi.install_hexrays_callback(hexrays_events_callback_my)